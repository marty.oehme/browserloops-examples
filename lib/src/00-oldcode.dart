import 'dart:html' as html;
import 'dart:math' as math;

html.CanvasRenderingContext2D ctx;
html.CanvasElement el;

math.Random rng = new math.Random();
int GRIDNUM = 10;
int animID = 0;

// These are the functions to start the animation and stop the animation.
// (They are connected to the start/stop buttons in the demo setup.)
var _stop = () => html.window.cancelAnimationFrame(animID);
var _start = () => html.window.requestAnimationFrame(eventloop);

// This is what ticks every frame of the animation.
void eventloop(currentTime) {
  ctx.setFillColorRgb(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
  int BRICKSIZE = (el.width ~/ GRIDNUM);
  int x = rng.nextInt(GRIDNUM) * BRICKSIZE;
  int y = rng.nextInt(GRIDNUM) * BRICKSIZE;
  ctx.fillRect(x, y, (BRICKSIZE), (BRICKSIZE));

  animID = html.window.requestAnimationFrame(
      eventloop); // recursive call to our eventloop - calls it forever as soon as browser is willing to give us his time
}

void main() {
  _demoSetup();
}

//buttons and canvas setup
void _demoSetup() {
  el = new html.CanvasElement(width: 480, height: 480);
  html.querySelector('#basic_output').append(el);
  ctx = el.getContext('2d');
  var _clear = () {
    _stop();
    ctx.setFillColorRgb(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
    ctx.fillRect(0, 0, el.width, el.height);
    ctx.setFillColorRgb(rng.nextInt(255), rng.nextInt(255), rng.nextInt(255));
  };
  var _changeGrid = (int delta) {
    _clear();
    int newgrid = GRIDNUM + delta;
    GRIDNUM = (newgrid == 0) ? GRIDNUM : newgrid;
    _start();
  };
  _clear();

  html.querySelector('#basic_start').onClick.listen((e) {
    _stop();
    _start();
  });
  html.querySelector('#basic_stop').onClick.listen((e) => _stop());
  html.querySelector('#basic_reset').onClick.listen((e) => _clear());
  html.querySelector('#basic_plus').onClick.listen((e) => _changeGrid(-5));
  html.querySelector('#basic_minus').onClick.listen((e) => _changeGrid(5));
}
