import 'dart:html';

import 'package:browserloop/game/Game.dart';
import 'package:browserloop/game/LoopExample.dart';

class FixedLoopVariableRender implements LoopExample, VariableUpdates {
  double MS_PER_UPDATE = 1000.0;
  static final double SAFE_GUARD = 500.0;
  Stopwatch elapsed = new Stopwatch();
  double lag = 0.0;
  Game game;
  num id;

  FixedLoopVariableRender(this.game);

  void eventloop(num time) {
    lag += elapsed.elapsedMilliseconds;
    elapsed.reset();

    while (lag >= MS_PER_UPDATE && elapsed.elapsedMilliseconds < SAFE_GUARD) {
      update();
      lag -= MS_PER_UPDATE;
    }
    render(lag / MS_PER_UPDATE);
    id = window.requestAnimationFrame(eventloop);
  }

  void update() {
    game.update();
  }

  void render(double interp) {
    game.draw(interp);
  }

  // CONTROLS ON THE EXAMPLE PAGE
  // NOT NECESSARY FOR LOOP ITSELF
  void stop() {
    elapsed.stop();
    window.cancelAnimationFrame(id);
  }

  void start() {
    elapsed.start();
    elapsed.reset();
    window.requestAnimationFrame(eventloop);
  }

  @override
  void setUpdates(double updateRate) {
    MS_PER_UPDATE = updateRate;
  }
}
