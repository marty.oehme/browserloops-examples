import 'dart:html';
import 'package:browserloop/game/Game.dart';
import 'package:browserloop/game/LoopExample.dart';

/// The most basic update loop possible.
///
/// Note that this is technically cheating since it is using
/// animationFrames to improve browser performance for the examples in the
/// blog post. The real, actual simplest code is in file 01-WhileLoop.dart
class WhileLoop implements LoopExample {
  Game game;
  num id;

  WhileLoop(Game this.game);

  void eventloop(num time) {
    game.update();
    game.draw();

    id = window.requestAnimationFrame(eventloop);
  }

  void stop() {
    window.cancelAnimationFrame(id);
  }

  void start() {
    window.requestAnimationFrame(eventloop);
  }
}
