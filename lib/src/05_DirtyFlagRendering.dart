import 'dart:html';

import 'package:browserloop/game/Game.dart';
import 'package:browserloop/game/LoopExample.dart';

class DirtyFlagRender implements LoopExample, VariableUpdates {
  double MS_PER_UPDATE = 1000.0;
  static final double SAFE_GUARD = 500.0;
  Stopwatch elapsed = new Stopwatch();
  double lag = 0.0;
  Game game;
  num id;
  bool dirty = true;

  DirtyFlagRender(this.game);

  void eventloop(num time) {
    lag += elapsed.elapsedMilliseconds;
    elapsed.reset();

    while (lag >= MS_PER_UPDATE && elapsed.elapsedMilliseconds < SAFE_GUARD) {
      update();
      lag -= MS_PER_UPDATE;
    }
    if (dirty) render(lag / MS_PER_UPDATE);
    id = window.requestAnimationFrame(eventloop);
  }

  void update() {
    game.update();
    // This is for demonstration purposes. It will set the dirty flag once every
    // update. Usually should be set this deeper in the individual logic units
    // where a change happens and moved through the program from there.
    dirty = true;
  }

  void render(double interp) {
    game.draw(interp);
    dirty = false;
  }

  // CONTROLS ON THE EXAMPLE PAGE
  // NOT NECESSARY FOR LOOP ITSELF
  void stop() {
    elapsed.stop();
    window.cancelAnimationFrame(id);
  }

  void start() {
    elapsed.start();
    elapsed.reset();
    window.requestAnimationFrame(eventloop);
  }

  @override
  void setUpdates(double updateRate) {
    MS_PER_UPDATE = updateRate;
  }
}
