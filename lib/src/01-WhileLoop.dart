import 'package:browserloop/game/Game.dart';
import 'package:browserloop/game/LoopExample.dart';

/// The most basic update loop possible.
///
/// Best not to use this loop since it has a tendency to
/// crash your browser and not load correctly at all.
/// The way to get this to run would be the setInterval method
/// simple is either through setInterval in Javascript:
/// https://www.w3schools.com/howto/howto_js_animate.asp
/// or through the Timer API in Dart, especially
/// with Timer.periodic(16, callbackFunction);
/// To see how I got it running for the example,
/// see file 02-AnimationFrameWhile.dart
class SimpleLoop implements LoopExample {
  Game game;
  bool running = false;

  SimpleLoop(Game this.game);

  void eventloop() {
    while (running) {
      update();
    }
  }

  void update() {
    game.update();
    game.draw();
  }

  // Starting and stopping the loop for the example page
  void stop() {
    running = false;
  }

  void start() {
    eventloop();
    running = true;
  }
}
