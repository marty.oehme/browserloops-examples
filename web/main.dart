import 'dart:html';

import 'package:browserloop/game/Game.dart';
import 'package:browserloop/game/LoopExample.dart';
import 'package:browserloop/src/02-AnimationFrameWhile.dart';
import 'package:browserloop/src/03-VariableTimestep.dart';
import 'package:browserloop/src/04-FixedLoopVariableRender.dart';
import 'package:browserloop/src/05_DirtyFlagRendering.dart';

CanvasElement baseCanvas = new CanvasElement(width: 480, height: 480);

List<Example> examples = [
  Example(
      "While Loop Example", "#while_loop", new WhileLoop(new Game(baseCanvas))),
  Example("Variable Timestep", "#variable_timestep",
      new VariableTimestep(new Game(baseCanvas))),
  Example("Fixed Update, Variable Render", "#fixed_variable",
      new FixedLoopVariableRender(new Game(baseCanvas))),
  Example("Variable Render with Dirty Flag", "#dirty_flag",
      new DirtyFlagRender(new Game(baseCanvas)))
];
LoopExample active;

class Example {
  final String query;
  final String name;
  LoopExample loop;

  Example(this.name, this.query, this.loop);

  CanvasElement get canvas {
    if (loop != null) return loop.game.canvas;
    return new CanvasElement(width: 480, height: 480);
  }

  set canvas(CanvasElement canvas) {
    if (loop != null) loop.game.canvas = canvas;
  }
}

void main() {
  examples.forEach((ex) {
    resetExample(ex);
  });
}

void appendToDOM(Example example) {
  querySelector(example.query).append(example.canvas);
  example.canvas.onClick.forEach(activate);
}

void removeFromDOM(Example example) {
  querySelector(example.query).children.clear();
}

void resetExample(Example ex) {
  removeFromDOM(ex);
  if (ex.loop != null) ex.loop.stop();
  ex.canvas =
      new CanvasElement(width: ex.canvas.width, height: ex.canvas.height);
  createPlaceholder(ex);
  appendToDOM(ex);
}

void createPlaceholder(Example ex) {
  if (ex.canvas == null) return;

  // Clear Canvas
  CanvasRenderingContext2D ctx = ex.canvas.context2D;
  Point c = Point(ex.canvas.width / 2, ex.canvas.height / 2);
  ctx.setFillColorRgb(183, 20, 39);
  ctx.fillRect(0, 0, ex.canvas.width, ex.canvas.height);

  // Draw Play Button
  ctx.beginPath();
  ctx.moveTo(c.x - 25, c.y - 25);
  ctx.lineTo(c.x + 25, c.y + 25);
  ctx.lineTo(c.x - 25, c.y + 75);
  ctx.closePath();
  ctx.lineWidth = 10;
  ctx.setStrokeColorRgb(155, 155, 155);
  ctx.setFillColorRgb(255, 255, 255);
  ctx.stroke();
  ctx.fill();

  // Draw Text
  ctx.fillText(ex.name, c.x - ctx.measureText(ex.name).width / 2, c.y - 50);
}

void activate(MouseEvent e) {
  if (e.target is! CanvasElement) return;
  CanvasElement c = (e.target as CanvasElement);

  examples.forEach((Example ex) {
    if (ex.canvas == c) {
      resetExample(ex);
      addControls(ex);
    } else {
      resetExample(ex);
    }
  });
}

void addControls(Example ex) {
  DivElement group = new DivElement();
  ButtonElement randomButton = new ButtonElement()..text = "Random/Ordered";
  querySelector(ex.query).append(group
    ..append(new ButtonElement()
      ..text = "Start"
      ..onClick.listen((e) {
        ex.loop.stop();
        ex.loop.start();
      }))
    ..append(new ButtonElement()
      ..text = "Stop"
      ..onClick.listen((e) {
        ex.loop.stop();
      }))
    ..append(new ButtonElement()
      ..text = "+"
      ..onClick.listen((e) {
        ex.loop.game.changeGridSize(true);
      }))
    ..append(new ButtonElement()
      ..text = "-"
      ..onClick.listen((e) {
        ex.loop.game.changeGridSize(false);
      }))
    ..append(randomButton
      ..onClick.listen((e) {
        ex.loop.game.toggleRandomOrder();
      })));

  // Don't add controls which don't work anyways (for simple examples)
  if (examples.indexOf(ex) <= 0) return;
  VariableUpdates loop = (ex.loop as VariableUpdates);

  // Update Speed Slider
  group
    ..append(new LabelElement()
      ..htmlFor = "update_speed"
      ..innerHtml = "Updates per Second:")
    ..append(new InputElement(type: "range")
      ..id = "update_speed"
      ..min = "1"
      ..max = "50"
      ..value = "2"
      ..step = "1"
      ..onInput.listen((Event e) {
        loop.setUpdates(1000 / int.parse((e.target as InputElement).value));
      }));
  //    querySelector('#reset').onClick.listen((e) => ex.loop.game.reset());
}
